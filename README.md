# Automating Infra Management & App Deployments with Ansible

## Table of Contents

1. [Requirements](#requirements)
2. [Playbook 1](#playbook-1): Builds a Java artifact locally. Deploys as a new user on a DigitalOcean Ubuntu server.
3. [Playbook 2](#playbook-2): Builds a Java artifact locally. Pushes to a Sonatype Nexus repository.
4. [Playbook 3](#playbook-3): Automates dynamic provisioning and configuration of Jenkins EC2(s) w/ Node.js, npm, and Docker installed for pipeline builds.
5. [Playbook 4](#playbook-4): Same as 3, but creates EC2(s) use Ubuntu Server 22.04 LTS (HVM), SSD Volume Type, arm64 image.
6. [Playbook 5](#playbook-5): Configures EC2(s) to run Jenkins in a Docker container. Installs Node.js and npm, and ensure Docker is also available for pipeline builds.
7. [Playbook 6](#playbook-6): All in the same AWS VPC, creates a dedicated Ansible server that creates and configures 2 servers to deploy a Java-Gradle app and a MySQL database (both non-Dockerized). The DB is accessible ONLY within the VPC.
8. [Playbook 7](#playbook-7): Automates deploying 1 Java-Gradle app pod and 1 MySQL replica (both Dockerized) to a Kubernetes cluster. Creates K8s config files for the app and MySQL deployments and services, as well as a configMap and Secret to enable the app-DB connection. Also deploys Nginx-Ingress Controller with a rule allowing browser access to the Java app.
9. [Playbook 8](#playbook-8): Same as 7, but to improve the Java-Gradle app's availability, uses Helm to deploy 3 MySQL replicas.

## Requirements

To run Ansible playbooks, you must have Ansible installed on your local machine. On Mac, use Homebrew package manager to install globally: `brew install ansible`. On other operating systems, use Python's pip package manager to install for this project: `pip install ansible`.

Playbook-specific requirements will be noted below.

## Playbook 1

### Objectives:

Build a Java artifact locally. Deploy as a new user on a DigitalOcean Ubuntu server.

### Relevant Directories/ Files:

- `ansible.cfg`
- `inventory/hosts`
- `playbooks/1-build-deploy-jar.yaml`
- `build-tools-exercises`: Java application built with Gradle. This app requires Java v17 runtime. Its server runs on port 8080.

### Steps:

1. In `ansible.cfg`, uncomment (enable) the default settings for EXERCISE 1.
2. Provision a DigitalOcean Ubuntu droplet, SSD disk type, 2GB/ 1CPU, 50GB SSD disk, 2TB transfer.

- Create a SSH key pair Ansible will use to authenticate to the server. Download and store the private key safely in your local `.ssh` directory.
- Set firewall rules to allow your IP into port 22 and all inbound connections into port 8080.

3. In `inventory/hosts`, replace the current values with your server's public IP/hostname and your SSH private key location.
4. `cd playbooks` and run the playbook using the command `ansible-playbook 1-build-deploy-jar.yaml`.
5. Ansible will prompt you to input a name, which will be used to generate a Linux system user to run the Java app on the server.
6. Once the playbook run completes, pause 10 secs to allow the app server to start listening on port 8080. Load the app URL `[server_IP/hostname]:8080` in the browser to see the running app.

## Playbook 2

### Objectives:

Build a Java artifact locally. Push to a Sonatype Nexus repository.

### Relevant Directories/ Files:

- `ansible.cfg`
- `inventory/hosts`
- `playbooks/2-build-push-jar-nexus.yaml`
- `build-tools-exercises`: Java application built with Gradle. This app requires Java v17 runtime. Its server runs on port 8080.

### Steps:

1. In `ansible.cfg`, uncomment (enable) the default settings for EXERCISE 2.
2. Provision a DigitalOcean Ubuntu droplet, SSD disk type, 8GB/ 4CPUs, 160GB SSD disk, 5TB transfer. (Nexus requires ample memory to run.)

- Create a SSH key pair Ansible will use to authenticate to the server. Download and store the private key safely in your local `.ssh` directory.
- Set firewall rules to allow your IP into port 22 and all inbound connections into port 8081 (where Nexus will run).

3. In `inventory/hosts`, replace the current values with your server's public IP/hostname and your SSH private key location.
4. `cd playbooks` and run the playbook using the command `ansible-playbook 2-build-push-jar-nexus.yaml`.
5. Ansible will print out the password to sign into the Nexus UI as an `admin` user.
6. Once the playbook run completes, the Nexus app should be running. Load the Nexus URL `[server_IP/hostname]:8081` in the browser.
7. Sign In to the Nexus UI as the `admin` user with the printed password.
8. To view the uploaded Java app artifact in the Nexus UI, click Browse > `maven-snapshots` > component tree.

## Playbook 3

### Objectives:

Automate dynamic provisioning and configuration of Jenkins EC2(s) w/ Node.js, npm, and Docker installed for pipeline builds.

### Relevant Directories/ Files:

- `ansible.cfg`
- `inventory/jenkins-aws_ec2.yaml`
- `playbooks/3-install-jenkins-ec2-amznlinux.yaml`

### Steps:

1. Run `aws configure` locally. Provide the Access and Secret Keys of the IAM user account in which Ansible will manage resources. `python >= 3.6`, `boto3 >= 1.26.0`, and `botocore >= 1.29.0` must be installed locally for `amazon.aws` collection modules to run.
2. In the configured AWS account's EC2 service, manually create a SSH key pair Ansible will use to authenticate to the server(s). Download and store the private key safely in your local `.ssh` directory.
3. In `ansible.cfg`, uncomment (enable) the default settings for EXERCISE 3. Replace the current value with your SSH private key location.
4. In `inventory/jenkins-aws_ec2.yaml`, update the region(s) in which the AWS resources should be created.
5. In `3-install-jenkins-ec2-amznlinux.yaml`:

- Replace the playbook user's IP with your IP address (look up here: whatismyip.com).
- Replace the exact count of EC2s to the number of servers you will need, and the key name with the name of your SSH key pair in AWS.

6. `cd playbooks` and run the playbook using the command `ansible-playbook 3-install-jenkins-ec2-amznlinux.yaml`.
7. Ansible will print out the password to sign into the Jenkins UI as an `admin` user.
8. Once the playbook run completes, the Jenkins app should be running. Load the Jenkins URL `[server_IP/hostname]:8080` in the browser.
9. Sign In to the Jenkins UI as the `admin` user with the printed password.

## Playbook 4

### Objectives:

Same as 3, but created EC2(s) use Ubuntu Server 22.04 LTS (HVM), SSD Volume Type, arm64 image.

### Relevant Directories/ Files:

- `ansible.cfg`
- `inventory/jenkins-aws_ec2.yaml`
- `playbooks/4-install-jenkins-ec2-ubuntu.yaml`

### Steps:

1. Run `aws configure` locally. Provide the Access and Secret Keys of the IAM user account in which Ansible will manage resources. `python >= 3.6`, `boto3 >= 1.26.0`, and `botocore >= 1.29.0` must be installed locally for `amazon.aws` collection modules to run.
2. In the configured AWS account's EC2 service, manually create a SSH key pair Ansible will use to authenticate to the server(s). Download and store the private key safely in your local `.ssh` directory.
3. In `ansible.cfg`, uncomment (enable) the default settings for EXERCISE 4. Replace the current value with your SSH private key location.
4. In `inventory/jenkins-aws_ec2.yaml`, update the region(s) in which the AWS resources should be created.
5. In `4-install-jenkins-ec2-ubuntu.yaml`:

- Replace the playbook user's IP with your IP address (look up here: whatismyip.com).
- Replace the exact count of EC2s to the number of servers you will need, and the key name with the name of your SSH key pair in AWS.

6. `cd playbooks` and run the playbook using the command `ansible-playbook 4-install-jenkins-ec2-ubuntu.yaml`.
7. Ansible will print out the password to sign into the Jenkins UI as an `admin` user.
8. Once the playbook run completes, the Jenkins app should be running. Load the Jenkins URL `[server_IP/hostname]:8080` in the browser.
9. Sign In to the Jenkins UI as the `admin` user with the printed password.

## Playbook 5

### Objectives:

Configure EC2(s) to run Jenkins in a Docker container. Install Node.js and npm, and ensure Docker is also available for pipeline builds.

### Relevant Directories/ Files:

- `ansible.cfg`
- `inventory/jenkins-aws_ec2.yaml`
- `playbooks/5-*`

### Steps:

1. Run `aws configure` locally. Provide the Access and Secret Keys of the IAM user account in which Ansible will manage resources. `python >= 3.6`, `boto3 >= 1.26.0`, and `botocore >= 1.29.0` must be installed locally for `amazon.aws` collection modules to run.
2. In the configured AWS account's EC2 service, manually create a SSH key pair Ansible will use to authenticate to the server(s). Download and store the private key safely in your local `.ssh` directory.
3. In `ansible.cfg`, uncomment (enable) the default settings for EXERCISE 5. Set the remote user to `ec2-user` for `run-jenkins-container-ec2-amznlinux.yaml` or `ubuntu` for `run-jenkins-container-ec2-ubuntu.yaml`. Replace the current value with your SSH private key location.
4. In `inventory/jenkins-aws_ec2.yaml`, update the region(s) in which the AWS resources should be created.
5. In whichever playbook you're using:

- Replace the playbook user's IP with your IP address (look up here: whatismyip.com).
- Replace the exact count of EC2s to the number of servers you will need, and the key name with the name of your SSH key pair in AWS.

6. `cd playbooks` and run the playbook using the `ansible-playbook` command.
7. Ansible will print out the password to sign into the Jenkins UI as an `admin` user.
8. Once the playbook run completes, the Jenkins app should be running. Load the Jenkins URL `[server_IP/hostname]:8080` in the browser.
9. Sign In to the Jenkins UI as the `admin` user with the printed password.

## Playbook 6

### Objectives:

All in the same AWS VPC, create a dedicated Ansible server that creates and configures 2 servers to deploy a Java-Gradle app and a MySQL database (both non-Dockerized). The DB is accessible ONLY within the VPC.

### Relevant Directories/ Files:

- `ansible.cfg`
- `6-ansible.cfg`
- `inventory/ansible-aws_ec2.yaml`
- `playbooks/6-*`
- `java-mysql-app`: Java application built with Gradle wrapper. This app requires Java v17 runtime. Its server runs on port 8080.

### Steps:

1. Run `aws configure` locally. Provide the Access and Secret Keys of the IAM user account in which Ansible will manage resources. `python >= 3.6`, `boto3 >= 1.26.0`, and `botocore >= 1.29.0` must be installed locally for `amazon.aws` collection modules to run.
2. In the configured AWS account's EC2 service, manually create a SSH key pair. Download and store the private key safely in your local `.ssh` directory.
3. In `ansible.cfg`, uncomment (enable) the default settings for EXERCISE 6. Replace the current value with your SSH private key location.
4. In `6-ansible.cfg`, replace the current value with your SSH private key name (not location).
5. In `inventory/ansible-aws_ec2.yaml`, update the region(s) in which the AWS resources should be created.
6. In `playbooks/6-create-control-node.yaml`:

- Replace the playbook user's IP with your IP address (look up here: whatismyip.com).
- Replace the key name with the name of your SSH key pair in AWS.

7. `cd playbooks` and run the playbook using the `ansible-playbook 6-create-control-node.yaml` command.
8. Once the playbook run completes, manually SSH into the dedicated `ansible-server` EC2. Then run `ansible-playbook 6-create-app-db-servers.yaml` on the dedicated server.
9. Load the app URL `[server_IP/hostname]:8080` in the browser.
10. To clean up all resources, from the AWS Management Console:

- Terminate all EC2 instances.
- In `mysql-server-subnet-rtb`, remove explicit subnet association w/ `mysql-server-subnet`.
- Delete subnet `mysql-server-subnet`.
- Delete route table `mysql-server-subnet-rtb`.
- Delete NAT gateway `mysql-server-subnet-nat`.
- Delete security groups `ansible-server-rules`, `mysql-server-rules`, and `app-server-rules`.

## Playbook 7

### Objectives:

Automate deploying 1 Java-Gradle app pod and 1 MySQL replica (both Dockerized) to a Kubernetes cluster. Create K8s config files for the app and MySQL deployments and services, as well as a configMap and Secret to enable the app-DB connection. Also deploy Nginx-Ingress Controller with a rule allowing browser access to the Java app.

### Relevant Directories/ Files:

- `7-k8s-components/*`
- `playbooks/7-k8s-cluster-mysql-deploy.yaml`
- `java-mysql-app`: Java application built with Gradle wrapper. This app requires Java v17 runtime. Its server runs on port 8080. This playbook deploys this app on AWS EKS.

### Steps:

1. Run `aws configure` locally. Provide the Access and Secret Keys of the IAM user account in which Ansible will manage resources.
2. Install [`kubectl`](https://kubernetes.io/docs/tasks/tools/#kubectl), [`eksctl`](https://eksctl.io/installation/), and [`helm`](https://helm.sh/docs/intro/install/) locally. Ensure you have a local kubeconfig (`.kube/config`) file.
3. In `7-k8s-components/eks-cluster.yaml`, update the region in which the cluster should be created.
4. `cd playbooks` and run the playbook using the `ansible-playbook 7-k8s-cluster-mysql-deploy.yaml` command.
5. Ansible will print the URL to load the Java app in the browser.
6. After executing the playbook, wait a few minutes. It takes a little time before the Java app loads in the browser.
7. To delete the cluster and all deployed resources, uncomment the last play `Delete the EKS cluster and update the kubeconfig` in the playbook and re-run `ansible-playbook` command.

## Playbook 8

### Objectives:

Same as 7, but to improve the Java-Gradle app's availability, use Helm to deploy 3 MySQL replicas.

### Relevant Directories/ Files:

- `8-k8s-components/*`
- `playbooks/8-k8s-cluster-mysql-helm.yaml`
- `java-mysql-app`: Java application built with Gradle wrapper. This app requires Java v17 runtime. Its server runs on port 8080. This playbook deploys this app on AWS EKS.

### Steps:

1. Run `aws configure` locally. Provide the Access and Secret Keys of the IAM user account in which Ansible will manage resources.
2. Install [`kubectl`](https://kubernetes.io/docs/tasks/tools/#kubectl), [`eksctl`](https://eksctl.io/installation/), and [`helm`](https://helm.sh/docs/intro/install/) locally. Ensure you have a local kubeconfig (`.kube/config`) file.
3. In `8-k8s-components/eks-cluster.yaml`, update the region in which the cluster should be created.
4. `cd playbooks` and run the playbook using the `ansible-playbook 8-k8s-cluster-mysql-helm.yaml` command.
5. Ansible will print the URL to load the Java app in the browser.
6. After executing the playbook, wait a few minutes. It takes a little time before the Java app loads in the browser.
7. To delete the cluster and all deployed resources, uncomment the last play `Delete the EKS cluster and update the kubeconfig` in the playbook and re-run `ansible-playbook` command.
