---
- name: Create an EKS cluster and update the kubeconfig file's current-context to the created cluster
  hosts: localhost
  tasks:
    - ansible.builtin.command:
        cmd: eksctl create cluster -f eks-cluster.yaml
        chdir: ../8-k8s-components
      register: create_cluster
      failed_when: '"AlreadyExistsException" in create_cluster.stdout'
      ignore_errors: true

- name: Install MySQL Helm chart deploying 3 MySQL replicas
  hosts: localhost
  vars:
    release: java-gradle-app-db
  tasks:
    - name: Add chart repository
      kubernetes.core.helm_repository:
        repo_name: bitnami
        repo_url: https://charts.bitnami.com/bitnami

    - kubernetes.core.helm:
        update_repo_cache: true
        release_name: '{{release}}'
        release_namespace: default
        chart_ref: bitnami/mysql
        values_files: ../8-k8s-components/mysql.yaml
        # waits until all Pods, PVCs, Services, and minimum number of Pods of a Deployment are in a ready state before marking the release as successful
        wait: true

    - name: Deploy the configmap to enable the app to connect to the DB
      kubernetes.core.k8s:
        definition:
          apiVersion: v1
          kind: ConfigMap
          metadata:
            name: proj-configmap
            namespace: default
          data:
            # Java-Gradle app will connect to MySQL's primary statefulset's svc
            DB_SERVER: '{{release}}-mysql-primary.default.svc.cluster.local'

    - name: Deploy the secret to enable the app to connect to the DB
      kubernetes.core.k8s:
        src: ../8-k8s-components/secret.yaml

- name: Install Nginx-Ingress Controller Helm chart
  hosts: localhost
  vars:
    release: java-gradle-app
  tasks:
    - name: Add chart repository
      kubernetes.core.helm_repository:
        repo_name: ingress-nginx
        repo_url: https://kubernetes.github.io/ingress-nginx

    - kubernetes.core.helm:
        update_repo_cache: true
        release_name: '{{release}}'
        release_namespace: default
        chart_ref: ingress-nginx/ingress-nginx
        wait: true

    - name: Get Nginx-Ingress controller service's LoadBalancer hostname
      kubernetes.core.k8s_info:
        api_version: v1
        kind: Service
        namespace: default
        name: '{{release}}-ingress-nginx-controller'
      register: controller_service
    - ansible.builtin.debug:
        var: controller_service.resources[0].status.loadBalancer.ingress[0].hostname

- name: Deploy the ingress rule and 3 Java-Gradle app pods
  hosts: localhost
  tasks:
    - kubernetes.core.k8s:
        definition:
          apiVersion: networking.k8s.io/v1
          kind: Ingress
          metadata:
            name: java-gradle-app-ingress-rule
            namespace: default
          spec:
            # Helm requirement: : https://artifacthub.io/packages/helm/ingress-nginx/ingress-nginx
            ingressClassName: nginx
            rules:
              # controller svc LoadBalancer / External hostname
              - host: '{{controller_service.resources[0].status.loadBalancer.ingress[0].hostname}}'
                # HTTP forwarding from host to a cluster-internal svc
                http:
                  paths:
                    - path: /
                      pathType: Prefix
                      backend:
                        service:
                          name: java-gradle-app-svc
                          port:
                            number: 8080

    - kubernetes.core.k8s:
        src: ../8-k8s-components/java-gradle-app.yaml

    - ansible.builtin.debug:
        msg: 'URL to load the Java app in the browser: {{controller_service.resources[0].status.loadBalancer.ingress[0].hostname}}'
#
# Run the following play to delete the cluster
#
# - name: Delete the EKS cluster and update the kubeconfig
#   hosts: localhost
#   tasks:
#     - ansible.builtin.command: eksctl delete cluster --name java-mysql-cluster
