---
- name: Provision Java app EC2s
  hosts: localhost
  tasks:
    - name: Create sec group for Java app
      amazon.aws.ec2_security_group:
        name: app-server-rules
        description: Java app server firewalls
        rules:
          - ports: 22
            cidr_ip: 0.0.0.0/0
          - ports: 8080
            cidr_ip: 0.0.0.0/0
      register: app_sec_group
    - ansible.builtin.debug:
        var: app_sec_group.group_id

    - name: Create new EC2 using app-server-rules sec group
      amazon.aws.ec2_instance:
        exact_count: 1
        name: app-server
        state: started
        # AMZN Linux 2023 arm64
        image_id: ami-0bbebc09f0a12d4d9
        instance_type: t4g.large
        network:
          assign_public_ip: true
        security_group: '{{app_sec_group.group_id}}'
        key_name: ansible

- name: Provision MySQL DB EC2s
  hosts: localhost
  tasks:
    # This module gets the configured region's default VPC's id
    - amazon.aws.ec2_vpc_net_info:
      register: all_vpcs_info
    - ansible.builtin.debug:
        var: all_vpcs_info.vpcs[0].id

    # This module gets the id of any default subnet in the default VPC. All default subnets are public.
    - amazon.aws.ec2_vpc_subnet_info:
        filters:
          vpc-id: '{{all_vpcs_info.vpcs[0].id}}'
      register: all_subnets_info
    - ansible.builtin.debug:
        var: all_subnets_info.subnets[0].subnet_id

    - name: Create a private subnet in which to launch MySQL EC2s
      amazon.aws.ec2_vpc_subnet:
        vpc_id: '{{all_vpcs_info.vpcs[0].id}}'
        cidr: 172.31.96.0/20
        tags:
          Name: mysql-server-subnet
      register: mysql_server_subnet
    - ansible.builtin.debug:
        var: mysql_server_subnet.subnet.id

    - name: Create a new NAT gateway in a PUBLIC subnet and allocate a new EIP
      amazon.aws.ec2_vpc_nat_gateway:
        subnet_id: '{{all_subnets_info.subnets[0].subnet_id}}'
        if_exist_do_not_create: true
        # waits 320 secs for NAT to be created
        wait: true
        tags:
          Name: mysql-server-subnet-nat
      register: mysql_server_subnet_nat
    - ansible.builtin.debug:
        var: mysql_server_subnet_nat.nat_gateway_id

    - name: Create a NAT-protected route table for mysql-server-subnet
      amazon.aws.ec2_vpc_route_table:
        vpc_id: '{{all_vpcs_info.vpcs[0].id}}'
        routes:
          - dest: 0.0.0.0/0
            gateway_id: '{{mysql_server_subnet_nat.nat_gateway_id}}'
        # creates an explicit association b/w the private subnet created above and this route table
        subnets: '{{mysql_server_subnet.subnet.id}}'
        tags:
          Name: mysql-server-subnet-rtb

    - name: Create sec group for MySQL DB
      amazon.aws.ec2_security_group:
        name: mysql-server-rules
        description: MySQL server firewalls
        rules:
          - ports: 22
            cidr_ip: 0.0.0.0/0
          - ports: 3306
            group_id: '{{app_sec_group.group_id}}'
      register: mysql_sec_group
    - ansible.builtin.debug:
        var: mysql_sec_group.group_id

    - name: Create new EC2 in mysql-server-subnet using mysql-server-rules sec group
      amazon.aws.ec2_instance:
        exact_count: 1
        name: mysql-server
        state: started
        # Ubuntu Server 22.04 LTS (HVM), SSD Volume Type, arm64
        image_id: ami-05d47d29a4c2d19e1
        instance_type: t4g.large
        vpc_subnet_id: '{{mysql_server_subnet.subnet.id}}'
        network:
          assign_public_ip: false
        security_group: '{{mysql_sec_group.group_id}}'
        key_name: ansible

    - name: Refresh inventory (switching from localhost to dyn inventory hosts at runtime)
      ansible.builtin.meta: refresh_inventory

# automate running the rest of playbooks from the Ans dedicated server

- name:
  ansible.builtin.import_playbook: 6-configure-ec2-mysql.yaml

- name:
  ansible.builtin.import_playbook: 6-configure-ec2-java-app.yaml
