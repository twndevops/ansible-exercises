---
- name: Build the Java-MySQL app artifact
  hosts: localhost
  tasks:
    - name: Remove old build dir (if any)
      ansible.builtin.command:
        cmd: ./gradlew clean
        chdir: ../java-mysql-app

    - name: Build new JAR file
      ansible.builtin.command:
        cmd: ./gradlew build
        chdir: ../java-mysql-app

- name: Provision a dedicated Ansible EC2
  hosts: localhost
  tasks:
    - name: Create sec group
      amazon.aws.ec2_security_group:
        name: ansible-server-rules
        description: Ansible server firewalls
        rules:
          - ports: 22
            # playbook user's IP
            cidr_ip: 172.56.68.251/32
      register: ansible_sec_group
    - ansible.builtin.debug:
        var: ansible_sec_group.group_id

    - name: Create new EC2 using ansible-server-rules sec group
      amazon.aws.ec2_instance:
        exact_count: 1
        name: ansible-server
        state: started
        # AMZN Linux 2023 arm64
        image_id: ami-0bbebc09f0a12d4d9
        instance_type: t4g.large
        network:
          # MUST be true so playbook user can SSH into the server to run the next playbook
          assign_public_ip: true
        security_group: '{{ansible_sec_group.group_id}}'
        key_name: ansible

    - name: Refresh inventory (switching from localhost to dyn inventory hosts at runtime)
      ansible.builtin.meta: refresh_inventory

- name: Prepare the Ansible dedicated server w/ needed tools, config files, and playbooks
  hosts: ansible_server
  become: true # ec2-user -> root
  tasks:
    - name: Update dnf repos cache, install ansible and python3-boto3
      ansible.builtin.dnf:
        pkg: [ansible, python3-boto3]
        state: latest
        update_cache: true

    - name: Copy JAR file to the Ans dedicated server
      ansible.builtin.copy:
        src: ../java-mysql-app/build/libs/bootcamp-java-mysql-project-1.0-SNAPSHOT.jar
        dest: /home/ec2-user
        owner: ec2-user
        group: ec2-user
        mode: u+x

    - name: Copy config files (required to execute the other playbooks) to the Ans dedicated server
      block:
        - name: Copy .aws dir and files for Ans to auth to AWS acct to create resources
          ansible.builtin.copy:
            src: ~/.aws
            dest: /home/ec2-user
            owner: ec2-user
            group: ec2-user
            directory_mode: 0755
        - ansible.builtin.file:
            path: /home/ec2-user/.aws/config
            mode: 0600
        - ansible.builtin.file:
            path: /home/ec2-user/.aws/credentials
            mode: 0600

        - name: Copy AND rename 6-ansible.cfg to set Ans dedicated server's default Ans config
          ansible.builtin.copy:
            src: ../6-ansible.cfg
            dest: /home/ec2-user/ansible.cfg
            owner: ec2-user
            group: ec2-user
            mode: preserve

        - name: Copy AWS dynamic inventory plugin config to group MySQL and Java app hosts
          ansible.builtin.copy:
            src: ../inventory/ansible-aws_ec2.yaml
            dest: /home/ec2-user
            owner: ec2-user
            group: ec2-user
            mode: preserve

        - name: Copy SSH private key for Ans to SSH into MySQL and Java app hosts
          ansible.builtin.copy:
            src: ~/.ssh/ansible.pem
            dest: /home/ec2-user/.ssh/
            owner: ec2-user
            group: ec2-user
            mode: preserve

    - name: Copy playbooks to the Ans dedicated server
      block:
        - ansible.builtin.copy:
            src: ./6-create-app-db-servers.yaml
            dest: /home/ec2-user
            owner: ec2-user
            group: ec2-user
            mode: preserve
        - ansible.builtin.copy:
            src: ./6-configure-ec2-mysql.yaml
            dest: /home/ec2-user
            owner: ec2-user
            group: ec2-user
            mode: preserve
        - ansible.builtin.copy:
            src: ./6-configure-ec2-java-app.yaml
            dest: /home/ec2-user
            owner: ec2-user
            group: ec2-user
            mode: preserve

- name: Install Ansible role for an Ubuntu MySQL server
  hosts: ansible_server
  tasks:
    - ansible.builtin.command: ansible-galaxy role install geerlingguy.mysql
