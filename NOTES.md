#### Notes for "Module 15 - Configuration Management w/ Ansible" exercises

ALL EXERCISES:

- Uncomment / enable default settings in `ansible.cfg` before executing respective playbook(s).

EXERCISE 1: Build a Java artifact locally. Deploy as a new user on an Ubuntu server.

- Provision 1 DigitalOcean droplet, Ubuntu distro, SSD disk type.

  - Specs: 2GB/ 1CPU, 50GB SSD disk, 2TB transfer.
  - Set firewall rules to allow your IP into port 22 and all inbound connections to port 8080.

- In `inventory/hosts`, add:

  - droplet's public IP/ hostname
  - SSH private key location
  - connecting/ executing user

- Instead of the [repo provided in the exercises](https://gitlab.com/devops-bootcamp3/java-gradle-app), used [build-tools-exercises](https://gitlab.com/twn-devops-bootcamp/latest/04-build-tools/build-tools-exercises). App requires v17 Java runtime and runs / listens on port 8080.

- NOTES on play `Build the Java-Gradle app artifact`:

  - `gradle clean` task does not error out if there is not a `build` dir in proj.

- NOTES on play `Prepare hosts to run the app`:

  - If an old JAR file is removed in the `Remove old JAR file` task, the process running the old JAR file must be stopped.
    - ISSUE: `killall java` errors out if there's no running app process. The app only runs for 1 hr before timing out (see play `Start the app as the new Linux user` > `async: 3600` seconds), so running the playbook again after 1 hr will cause the `killall`task to error out.
      - WORKAROUND: After the run errors out, re-run the playbook and the `killall java` task is skipped because there is no old JAR file to remove.

- NOTES on play `Start the app as the new Linux user`:

  - `java -jar build-tools-exercises-1.0-SNAPSHOT.jar &` runs the app as a background process that continues even if the Terminal session breaks / ends.
  - The newly created user's home dir must be specified as the dir from which to execute the `cmd` or the JAR file will not be accessible.
  - `poll: 0` allows Ansible to concurrently execute the remaining playbook tasks w/o waiting for the current task to finish.

- NOTES on play `Check app is listening on port 8080`:

  - Pause 10 secs to allow the app server to start listening on the port.
  - Non-root users see only their processes from `-p` output (root sees all).

- Load the URL `[droplet_IPv4]:8080` in the browser to see the running app.
  - ISSUE: App loads successfully on `localhost:8080` when run locally. When run on the droplet, app loads 404 Not Found `Whitelabel Error Page: This application has no explicit mapping for /error, so you are seeing this as a fallback`.

EXERCISE 2: Build a Java artifact locally. Push to a Sonatype Nexus repository.

- Provision 1 DigitalOcean droplet, Ubuntu distro, SSD disk type.

  - Nexus app needs ample memory space. Specs: 8GB/ 4CPUs, 160GB SSD disk, 5TB transfer.
  - Set firewall rules to allow your IP into port 22 and all inbound connections to port 8081.

- Nexus requires v8 Java runtime.

- NOTES on play `If nexus dir is absent, download and unpack the Nexus installer`:

  - `https://download.sonatype.com/nexus/3/latest-unix.tar.gz` fetches an executable w/ this format: `nexus-3.65.0-02-unix.tar.gz`.

- NOTES on play `Configure a Nexus service user to run the app`:

  - It's best practice to create a new Linux service user to run a service.

- - Once the Nexus app is running, load `[droplet_IPv4]:8081` in the browser.

- NOTES on play `Print password to sign into the Nexus UI as an admin user`:

  - Sign In to the Nexus UI as `admin` user w/ printed password.

  - On the first playbook run, Ansible will pass the printed password to the command to upload the JAR to the Nexus repo. See the uploaded artifact in Nexus UI > Browse > `maven-snapshots` > component tree.

    - Nexus requires `admin` to reset the password after the first UI login. If the playbook must be re-run to upload multiple artifacts, update `Upload new JAR file to maven-snapshots repo` play `password` value to be the reset password.

- NOTES on play `Upload new JAR file to Nexus repo`:

  - Maven 2 format repos allow direct upload of assets using HTTP PUT.

    - Mapped the following command to the module. MUST use this format for the command to work!

    ```
    curl -v -u admin:admin --upload-file build-tools-exercises-1.0-SNAPSHOT.jar http://{{ansible_facts.all_ipv4_addresses[0]}}:8081/repository/maven-snapshots/com/example/build-tools-exercises/1.0-SNAPSHOT/build-tools-exercises-1.0-SNAPSHOT.jar
    ```

    - [Direct upload command reference](https://support.sonatype.com/hc/en-us/articles/115006744008-How-can-I-programmatically-upload-files-into-Nexus-3#DirectUploadusingHTTPPUTtotheRepositoryPath)

    - Got `405 Method Not Allowed` error attempting to POST the `url`: `"allow": "GET,HEAD,PUT,DELETE"`.

  - `body_format: json` prevents `HTTP Error 500: Server Error"`.

  - `status_code: 201` sets the expected success status code to 201. The module errors out if it receives anything other than 200.

    - 201 Created indicates the request created a resource.

EXERCISE 3: Automate dynamic provisioning and configuration of Jenkins EC2(s) w/ Node.js, npm, and Docker installed for pipeline builds.

- Configured `aws_ec2` dynamic inventory plugin to fetch info on created EC2(s).

  - See `ansible.cfg` and `jenkins-aws_ec2.yaml` for plugin configuration.
  - Run `ansible-inventory --graph` to see Ansible-managed nodes.

- NOTES on play `Configure firewalls and start EC2(s)`:

  - For Ansible to create and manage AWS resources from localhost (control node):

    - `AWS_REGION`, `AWS_ACCESS_KEY_ID`, and `AWS_SECRET_ACCESS_KEY` should be configured in local `.aws` dir.
    - `python >= 3.6`, `boto3 >= 1.26.0`, and `botocore >= 1.29.0` must be installed locally for `amazon.aws` collection modules to run.

  - The security group created / assigned to the EC2s must allow all browser traffic to port `8080` (where Jenkins runs) for Jenkins UI to load in a browser.

  - NOTE: In AWS, public IP auto-assignment is enabled at instance-level, but public hostname auto-assigment is enabled at VPC-level.

  - Created EC2(s) use AMZN Linux 2023 arm64 image.

  - Before Ansible proceeds to the next play:
    - EC2 instances must exist AND be running AND wait for status checks to report OK.
    - MUST refresh (i.e. force reload of) inventory when new hosts (`jenkins-server` EC2(s)) are created, or the next play will return `skipping: no hosts matched` for `aws_ec2` group.
      - Hosts are matched before any play tasks are run.

- NOTES on play `Prepare Jenkins servers`:

  - Converted [instructions to install Jenkins on AWS](https://www.jenkins.io/doc/tutorials/tutorial-for-installing-jenkins-on-AWS/) to tasks.

  - [`community.general.jenkins*` modules](https://docs.ansible.com/ansible/latest/collections/community/general/index.html) require `python-jenkins` to be installed to perform actions in Jenkins.

- To confirm Jenkins is running, load `[EC2_public_dns_name]:8080` in your browser.

EXERCISE 4: Same as 3, but created EC2(s) use Ubuntu Server 22.04 LTS (HVM), SSD Volume Type, arm64 image.

- NOTES on play `Prepare Jenkins servers`:

  - Converted [instructions to install Jenkins on Debian/Ubuntu](https://www.jenkins.io/doc/book/installing/linux/#debianubuntu) to tasks.

    - ISSUE: Following module threw a `Malformed list` error when run:

      ```
      # `apt_repository` module dependency `python3-apt` is pre-installed on Ubuntu EC2
      ansible.builtin.apt_repository:
        repo: deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] https://pkg.jenkins.io/debian-stable binary/
        filename: jenkins
      ```

      - WORKAROUND: Used `shell` module to execute command verbatim.

  - MUST update apt repos cache after adding Jenkins repository and before trying to install Jenkins, or apt pkg manager will be unable to find the repository to do the install.

  - ISSUE: Installing `jenkins` in `install Java and other pkgs` task threw error `jenkins needrestart is being skipped since dpkg has failed`.
    - WORKAROUND: Installed in a later / separate task using verbatim command.

EXERCISE 5: Configure EC2(s) to run Jenkins in a Docker container. Install Node.js and npm, and ensure Docker is also available for pipeline builds.

- In `ansible.cfg`, set `remote_user` to `ec2-user` for `run-jenkins-container-ec2-amznlinux.yaml` and `ubuntu` for `run-jenkins-container-ec2-ubuntu.yaml`.

- NOTES on play `Install and run Docker`:

  - To run Jenkins in a container, Docker must be installed on the EC2.

    - ISSUE: Adding `ec2-user` / `ubuntu` to `docker` group didn't help to start Docker daemon.
      - WORKAROUND: To start the daemon, set `docker.sock`'s `owner` to `ec2-user` / `ubuntu`.

  - There are many (complex) setups available to run Docker cmds in the Jenkins container: [create Jenkins Dockerfile / image that has Docker CLI](https://www.jenkins.io/doc/book/installing/docker/#downloading-and-running-jenkins-in-docker), [DinD](https://devopscube.com/run-docker-in-docker/), etc. This playbook is not for testing (not production) purposes, so there's no need for a complex setup.

    - ISSUE: Adding `jenkins` user to `docker` group didn't allow the containerized Jenkins service to talk to the Docker daemon running on the EC2.
      - WORKAROUND: Updated `docker.sock` permissions from 0660 (`srw-rw---- ec2-user docker`) to 0666 (`srw-rw-rw- ec2-user docker`), which allowed the Jenkins service to access the daemon through the `docker.sock` volume mounted to its container (see `Start Jenkins container` task).

- NOTES on play `Start Jenkins as a container and install all tools for pipeline builds`:

  - [Image documentation](https://github.com/jenkinsci/docker/blob/master/README.md#connecting-agents).

  - `/var/jenkins_home` is the `jenkins` user's home dir in the container.

  - Jenkins image has a Java runtime pre-installed.

  - `apt update` before running `apt search` / `apt install`, or the search will yield no results / install(s) will fail.

  - Jenkins service installed on EC2 stores initialAdminPassword at `/var/lib/jenkins/secrets/initialAdminPassword`. Jenkins container stores it at `/var/jenkins_home/secrets/initialAdminPassword`.

EXERCISE 6: All in the same AWS VPC, create a dedicated Ansible server that creates and configures 2 servers to deploy a Java-Gradle app and a MySQL database (both non-Dockerized). The DB should be accessible ONLY within the VPC.

TL;DR / Quickstart:

1. Run `ansible-playbook playbooks/6-create-control-node.yaml` locally.
2. SSH into created `ansible-server` EC2.
3. Run `ansible-playbook 6-create-app-db-servers.yaml`. That's it!

NOTES applicable across playbooks:

- All AWS resources are created in the acct's configured default region's default VPC.

  - `mysql-server` EC2(s) are launched in a new private subnet created in the default VPC.
    - `mysql_server_subnet` private subnet is explicitly associated with the `mysql-server-subnet-rtb` route table.
  - All other resources are launched in (or associated with) AND auto-assigned public IPv4s by the default VPC's default subnets.
    - `mysql-server` EC2(s) are NOT assigned public IPs / hostnames. `ansible-server` and `app-server` EC2(s) are.

- All created security groups have a default all-out egress rule.

Regarding playbook `6-create-control-node.yaml`:

- This playbook should be executed locally.

  - The Ansible configuration (i.e. defaults) is in `ansible.cfg` > Exercise 6.
  - `hosts: localhost` refers to the playbook user's local machine.

- NOTES on play `Build the Java-MySQL app artifact`:

  - The app uses Gradle wrapper, so working project commands use `./gradlew`, NOT `gradle`.

- NOTES on play `Provision a dedicated Ansible EC2`:

  - `ansible-server` is assigned a public IP / hostname to allow the playbook user to SSH into the server manually to execute the next playbook. Manually SSH'ing using a private IP or hostname fails, even if all IPs are allowed to connect to port 22, b/c the playbook user's machine is outside the VPC where AWS resources are created.
    - NOTE: If no public hostname is available, Ansible uses the private hostname to SSH into a server.

- NOTES on play `Prepare remote Ansible control node w/ needed tools, config files, and playbooks`:

  - To run `amazon.aws` modules in later playbooks, the Ans dedicated server must have `python3` (pre-installed on EC2 image), `boto3`, and `botocore` installed.

- NOTES on play `Install Ansible role for an Ubuntu MySQL server`:

  - It's the last play b/c Ansible must be installed on the server before running an `ansible-galaxy` cmd.
  - The cmd installs the `mysql` role in `/home/ec2-user/.ansible/roles`.

Regarding playbook `6-create-app-db-servers.yaml`:

- This playbook should be executed on the dedicated `ansible-server` EC2 created in the previous playbook.

  - The Ansible configuration (i.e. defaults) is in `6-ansible.cfg` > Exercise 6. This file is copied to the Ansible dedicated server (`ansible-server`) and renamed to `ansible.cfg`.
    - By default, [Ans connects to remote hosts using the control node username](https://docs.ansible.com/ansible/latest/inventory_guide/connection_details.html#setting-a-remote-user). In this case, the connecting user is `ec2-user`.
  - `hosts: localhost` refers to the `ansible-server`.

- `app-server-rules` and `mysql-server-rules` security groups are both created with a rule allowing all IPs to SSH into port 22, so Ansible is able to connect to their EC2(s) to execute plays.

- NOTES on play `Provision Java app EC2s`:

  - The Java app's server runs / listens on port 8080. The app is publicly accessible via the browser at this port.

- NOTES on play `Provision MySQL DB EC2s`:

  - `mysql-server-subnet` is a private subnet created in or associated w/ an existing / public availability zone (i.e. `us-east-1e`) in the configured region's default VPC.

  - MySQL EC2(s) launched in the private subnet are NOT assigned public IPs / hostnames. The subnet routes all EC2 outbound connections to the Internet via a publicly connected NAT gateway. The NAT is created in a public subnet that routes all outbound connections to the Internet via an Internet gateway.

  - The MySQL service runs / listens on port 3306. Only `app-server` EC2(s) can connect to the DB at this port.

  - B/c `mysql-server` is NOT assigned a public IP / hostname and manually SSH'ing using a private IP / hostname fails, it's not possible to manually SSH into `mysql-server` EC2(s) b/c the playbook user's machine is outside the VPC where AWS resources are created.

    - NOTE: Since no public hostname is available, Ansible uses `mysql-server`'s private hostname to SSH in.

  - Later playbooks are imported / run from this playbook to automate the rest of the server configuration and app deployment steps.

Regarding playbook `6-configure-ec2-mysql.yaml`:

- NOTES on play `Prepare MySQL server`:

  - `community.mysql` collection is incl in Ansible install. Its modules are necessary for `mysql` role to work.

  - `mysql-server` EC2s are Ubuntu servers that can install the Ansible role to install and run MySQL service.
    - [`ubuntu` is the default username](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/managing-users.html) used to connect to an Ubuntu EC2. However, `6-ansible.cfg` specifies that the Ansible dedicated server connects to the MySQL and Java app EC2s using `remote_user=ec2-user`.
      - This play overrides `remote_user=ec2-user` with `remote_user=ubuntu`.
        - QUESTION: `6-ansible.cfg` specifies that the MySQL EC2's SSH private key location is `/home/ec2-user/.ssh/ansible.pem`, NOT `/home/ubuntu/.ssh/ansible.pem`. How does Ansible connect regardless?
    - To install the role, `ubuntu` user becomes `root`.

Regarding playbook `6-configure-ec2-java-app.yaml`:

- NOTES on play `Start the app`:

  - `java -jar bootcamp-java-mysql-project-1.0-SNAPSHOT.jar &` runs the app as a background process that continues even if the Terminal session breaks / ends.

    - `poll: 0` allows Ansible to concurrently execute the remaining playbook tasks w/o waiting for the current task to finish.

  - Env vars to set to connect the app to the DB can be found in `java-mysql-app/src/main/java/com/example/DatabaseConfig.java`.

    - Var values are set by the playbook user when configuring the `mysql` role / DB. The values the Java app uses to connect MUST MATCH the role's DB configuration.
    - NOTE: The DB env vars are NOT set in the `app-server` environment. (Manually SSH into the `app-server` and run `env` cmd to print all env vars.)
    - Used [`groups` magic variable](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_vars_facts.html#information-about-ansible-magic-variables) to pass `DB_SERVER` the `mysql-server`'s private hostname.

  - Load the URL `[app-server_public_hostname]:8080` in the browser to see the running app.

CLEANUP INSTRUCTIONS: From AWS UI,

- Terminate all EC2 instances.
- In `mysql-server-subnet-rtb`, remove explicit subnet association w/ `mysql-server-subnet`.
- Delete subnet `mysql-server-subnet`.
- Delete route table `mysql-server-subnet-rtb`.
- Delete NAT gateway `mysql-server-subnet-nat`.
- Delete security groups `ansible-server-rules`, `mysql-server-rules`, and `app-server-rules`.

FOLLOW-UP OPTIMIZATION: Automatically run all playbooks from `6-create-control-node.yaml` (i.e. reduce 3 steps in TL;DR to 1).

- Importing later playbooks into 1st one works, but the LOCAL MACHINE SSH private key file, dynamic config plugin file, ansible.cfg file, and playbooks are used to SSH into MySQL and Java app EC2s, NOT files and playbooks COPIED OVER to the Ansible dedicated server. This indicates that the local machine is executing the playbooks, NOT the dedicated server.

- Using `command` module to run Ans playbook cmd returns following stderr:

```
"[WARNING]:  * Failed to parse /home/ec2-user/ansible-aws_ec2.yaml with\nansible_collections.amazon.aws.plugins.inventory.aws_ec2 plugin: Failed to\ndescribe instances: Unable to locate credentials\n[WARNING]: Unable to parse /home/ec2-user/ansible-aws_ec2.yaml as an inventory\nsource\n[WARNING]: No inventory was parsed, only implicit localhost is available\n[WARNING]: provided hosts list is empty, only localhost is available. Note that\nthe implicit localhost does not match 'all'"
```

EXERCISE 7: Automate deploying 1 Java-Gradle app pod and 1 MySQL replica (both Dockerized) to a Kubernetes cluster. Create K8s config files for the app and MySQL deployments and services, as well as a configMap and Secret to enable the app-DB connection. Also deploy Nginx-Ingress Controller with a rule allowing browser access to the Java app.

- NOTES on `7-k8s-components/java-gradle-app.yaml`:

  - Cluster nodes use `x86_64` CPU arch by default. Built app image for `java-mysql-app` for multiple Linux architectures:

    ```
    docker buildx build --push --platform linux/arm64/v8,linux/amd64 --tag upnata/bootcamp-docker-java-mysql-project:1.0-ansible .
    ```

    - `upnata/bootcamp-docker-java-mysql-project` is a public DockerHub repo. FOLLOWUP ITEM: store / pull app image from a private repo.

  - Env vars to set to connect the app to the DB can be found in `java-mysql-app/src/main/java/com/example/DatabaseConfig.java`.

- NOTES on play `Create an EKS cluster and update the kubeconfig file's current-context to the created cluster`:

  - No available `kubernetes.core.k8s` module creates an EKS cluster. The module `kubernetes.core.k8s` cannot create a `ClusterConfig` component.

    - Error: `"Could not create API client: Invalid kube-config file. Expected object with name  in /Users/upasananatarajan/.kube/config/contexts list"`. The module expects the kubeconfig file to have a `current-context` set to an existing cluster and finds it empty.

  - Used `eksctl` to create a cluster AND update the kubeconfig file's `current-context` to the created cluster (i.e. no need to run `aws eks update-kubeconfig`).

    - Passed parameters using the `ClusterConfig` component as opposed to directly into the create cmd.

    - BEAR IN MIND: `us-east-1e` availability zone does NOT support `m5.large` nodes. The create cluster cmd automatically skips this zone. Specifying `availabilityZones: [us-east-1a, us-east-1b, us-east-1c, us-east-1d, us-east-1f]` in the create cmd directly works but in the `ClusterConfig` causes creation to fail.

  - If the cluster already exists, the play's task will error out. Used `failed_when` to define the `AlreadyExistsException` failure and `ignore_errors` for Ansible to ignore it and continue executing the playbook.
    - POSSIBLE / UNTESTED ISSUE: Ansible will ignore other cluster creation errors and continue playbook execution.

- NOTES on play `Delete the EKS cluster and update the kubeconfig`:

  - `eksctl delete cluster -f eks-cluster.yaml` barely works. The Java app breaks in the browser, but the cluster and nodegroup remain intact and CloudFormation stacks do NOT change status from CREATE_COMPLETE to DELETE_IN_PROGRESS.

  - If the playbook user updates the cluster name in `ClusterConfig`, they should also update the name in the delete cmd.
    - Ansible awaits completion of the delete cluster cmd.

EXERCISE 8: To improve the Java-Gradle app's availability, use Helm to deploy 3 MySQL replicas.

- NOTES applicable across plays:

  - To avoid confusion, use different `release_name`s for MySQL and Nginx-Ingress Controller chart installs.

- NOTES on play `Install MySQL Helm chart deploying 3 MySQL replicas`:

  - The values file is read from the target `hosts` filesystem (in this case, `localhost`).

- After executing the playbook, wait a few minutes. It takes a little time before the Java app loads in the browser.
